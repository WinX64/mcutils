package io.github.winx64.utils.chat;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

/**
 * A helper class for JSON formatted messages
 * 
 * @author WinX64
 *
 */
public final class Message {

	private final TextComponent root;
	private TextComponent child;

	/**
	 * Creates a new Message with the given initial text
	 * 
	 * @param text
	 *            The initial text
	 */
	public Message(String text) {
		this.root = new TextComponent(text);
		this.child = root;
	}

	/**
	 * Adds a hover event to the current message part, that displays the given
	 * text
	 * 
	 * @param text
	 *            The text
	 * @return Itself for chaining
	 */
	public Message hover(String text) {
		this.child.setHoverEvent(
				new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] { new TextComponent(text) }));
		return this;
	}

	/**
	 * Adds a hover event to the current message part, that executes the given
	 * action
	 * 
	 * @param action
	 *            The action
	 * @param components
	 *            The text components
	 * @return Itself for chaining
	 */
	public Message hover(HoverEvent.Action action, TextComponent... components) {
		this.child.setHoverEvent(new HoverEvent(action, components));
		return this;
	}

	/**
	 * Adds a click event to the current message part, that executes the given
	 * command
	 * 
	 * @param text
	 *            The command
	 * @return Itself for chaining
	 */
	public Message clickCommand(String text) {
		this.child.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, text));
		return this;
	}

	/**
	 * Adds a click event to the current message part, that suggests the given
	 * command
	 * 
	 * @param text
	 *            The command
	 * @return Itself for chaining
	 */
	public Message clickText(String text) {
		this.child.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, text));
		return this;
	}

	/**
	 * Adds a click event to the current message part, that opens the given URL
	 * 
	 * @param text
	 *            The URL
	 * @return Itself for chaining
	 */
	public Message clickUrl(String text) {
		this.child.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, text));
		return this;
	}

	/**
	 * Adds a click event to the current message part, that opens the given file
	 * 
	 * @param text
	 *            The file
	 * @return Itself for chaining
	 */
	public Message clickFile(String text) {
		this.child.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, text));
		return this;
	}

	/**
	 * Adds a click event to the current message part, that changes a book to
	 * the given page
	 * 
	 * @param text
	 *            The page
	 * @return Itself for chaining
	 */
	public Message clickPage(String text) {
		this.child.setClickEvent(new ClickEvent(ClickEvent.Action.CHANGE_PAGE, text));
		return this;
	}

	/**
	 * Adds a click event to the current message, that executes the given action
	 * 
	 * @param action
	 *            The action
	 * @param text
	 *            The text components
	 * @return Itself for chaining
	 */
	public Message click(ClickEvent.Action action, String text) {
		this.child.setClickEvent(new ClickEvent(action, text));
		return this;
	}

	/**
	 * Appends the given text to the existing message
	 * 
	 * @param text
	 *            The extra text
	 * @return Itself for chaining
	 */
	public Message more(String text) {
		this.child = new TextComponent(text);
		this.root.addExtra(child);
		return this;
	}

	/**
	 * Creates the TextComponent represented by this Message
	 * 
	 * @return The TextComponent object
	 */
	public TextComponent create() {
		return (TextComponent) root.duplicate();
	}
}
