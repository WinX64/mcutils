package io.github.winx64.utils.reflect;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.google.common.base.Preconditions;

/**
 * An utility class with methods revolving listeners
 * 
 * @author WinX64
 *
 */
public final class ListenerUtil {

	private static final Field JAVA_PLUGIN_FILE = Reflection.getField(JavaPlugin.class, "file");

	private ListenerUtil() {}

	/**
	 * Registers all the listener classes of the given plugin
	 * 
	 * @param plugin
	 *            The plugin
	 * @param arguments
	 *            The listeners' constructor arguments
	 */
	public static void registerListeners(JavaPlugin plugin, Object... arguments) {
		registerListeners(plugin, "", arguments);
	}

	/**
	 * Registers all the listener classes of the plugin on the given package
	 * 
	 * @param plugin
	 *            The plugin
	 * @param packageName
	 *            The name of the package
	 * @param arguments
	 *            The listeners' constructor arguments
	 */
	public static void registerListeners(JavaPlugin plugin, String packageName, Object... arguments) {
		Preconditions.checkNotNull(plugin, "plugin cannot be null");
		Preconditions.checkNotNull(packageName, "packageName cannot be null");
		Preconditions.checkNotNull(arguments, "arguments cannot be null");

		List<Class<? extends Listener>> listenerClassEntries = getJarListenerEntries(plugin, packageName);
		for (Class<? extends Listener> theClass : listenerClassEntries) {
			try {
				Listener listener = null;
				if (plugin.getClass() == theClass) {
					listener = (Listener) plugin;
				}
				nextElement: for (Constructor<?> constructor : theClass.getDeclaredConstructors()) {
					Class<?>[] parameters = constructor.getParameterTypes();
					if (parameters.length != arguments.length) {
						continue;
					}

					for (int i = 0; i < arguments.length; i++) {
						if (arguments[i] != null && !parameters[i].isAssignableFrom(arguments[i].getClass())) {
							continue nextElement;
						}
					}

					listener = (Listener) Reflection.createInstance(constructor, arguments);
					break;
				}

				if (listener == null) {
					continue;
				}
				Bukkit.getPluginManager().registerEvents(listener, plugin);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Returns all classes the implement {@link Listener} on the given plugin
	 * 
	 * @param plugin
	 *            The plugin
	 * @return The classes
	 */
	public static List<Class<? extends Listener>> getListenerClasses(JavaPlugin plugin) {
		Preconditions.checkNotNull(plugin, "plugin cannot be null");
		return getListenerClasses(plugin, null);
	}

	/**
	 * Returns all the classes that implement {@link Listener} on the given
	 * package of the plugin
	 * 
	 * @param plugin
	 *            The plugin
	 * @param packageName
	 *            The name of the package
	 * @return
	 */
	public static List<Class<? extends Listener>> getListenerClasses(JavaPlugin plugin, String packageName) {
		Preconditions.checkNotNull(plugin, "plugin cannot be null");
		return getJarListenerEntries(plugin, packageName);
	}

	/**
	 * Scans the jar file of the given plugin for all the available listener
	 * classes on the specified package
	 * 
	 * @param plugin
	 *            The plugin
	 * @param packageName
	 *            The name of the package
	 * @return The listener classes
	 */
	@SuppressWarnings("unchecked")
	private static List<Class<? extends Listener>> getJarListenerEntries(JavaPlugin plugin, String packageName) {
		List<Class<? extends Listener>> listenerClassEntries = new ArrayList<Class<? extends Listener>>();

		try {
			File pluginFile = (File) Reflection.getValue(JAVA_PLUGIN_FILE, plugin);
			try (JarFile pluginJarFile = new JarFile(pluginFile)) {
				Enumeration<JarEntry> entries = pluginJarFile.entries();
				while (entries.hasMoreElements()) {
					JarEntry entry = entries.nextElement();
					if (!entry.isDirectory()) {
						String fullClassPath = entry.getName().replace('/', '.');
						if (fullClassPath.endsWith(".class")) {
							String fullClassName = fullClassPath.substring(0, fullClassPath.lastIndexOf(".class"));
							Class<?> theClass = Reflection.getClass(fullClassName);
							if ((packageName == null || theClass.getPackage().getName().startsWith(packageName))
									&& Listener.class.isAssignableFrom(theClass)) {
								listenerClassEntries.add((Class<? extends Listener>) theClass);
							}
						}
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return listenerClassEntries;
	}
}
