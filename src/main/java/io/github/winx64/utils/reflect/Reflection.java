package io.github.winx64.utils.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.bukkit.Bukkit;

/**
 * A helper class with reflexive utility methods
 * 
 * @author WinX64
 *
 */
public final class Reflection {

	private static final String PACKAGE_VERSION;

	private static final String NMS_PACKAGE;

	private static final String OBC_PACKAGE;

	static {
		String[] entries = Bukkit.getServer().getClass().getPackage().getName().split("\\.");
		PACKAGE_VERSION = entries.length == 4 && entries[3].startsWith("v") ? entries[3] : null;
		NMS_PACKAGE = "net.minecraft.server." + PACKAGE_VERSION;
		OBC_PACKAGE = "org.bukkit.craftbukkit." + PACKAGE_VERSION;
	}

	private Reflection() {}

	/**
	 * Returns the package version prefix for both {@link net.minecraft.server}
	 * and {@link org.bukkit.craftbukkit}, or null if it's not formatted
	 * correctly or doesn't exist (pre 1.4)
	 * 
	 * @return The package version
	 */
	public static String getPackageVersion() {
		return PACKAGE_VERSION;
	}

	/**
	 * Returns a {@link net.minecraft.server} class
	 * 
	 * @param className
	 *            The name of the class
	 * @return The class, or null if it doesn't exist
	 */
	public static final Class<?> getNMSClass(String className) {
		return getClass(NMS_PACKAGE + "." + className);
	}

	/**
	 * Returns a {@link org.bukkit.craftbukkit} class
	 * 
	 * @param className
	 *            The name of the class
	 * @return The class, or null if it doesn't exist
	 */
	public static Class<?> getOBCClass(String className) {
		return getOBCClass(null, OBC_PACKAGE + "." + className);
	}

	/**
	 * Returns a {@link org.bukkit.craftbukkit} class that is inside a
	 * sub-package
	 * 
	 * @param subPackageName
	 *            The package the class is located
	 * @param className
	 *            The name of the class
	 * @return The class, or null if it doesn't exist
	 */
	public static Class<?> getOBCClass(String subPackageName, String className) {
		return getClass(OBC_PACKAGE + (subPackageName == null ? "" : "." + subPackageName) + "." + className);
	}

	/**
	 * Returns the specified inner class or the specified outer class
	 * 
	 * @param outerClass
	 *            The outer class
	 * @param className
	 *            The name of the inner class
	 * @return The inner class, or null if it doesn't exist
	 */
	public static Class<?> getInnerClass(Class<?> outerClass, String className) {
		for (Class<?> innerClass : outerClass.getDeclaredClasses()) {
			if (innerClass.getName().equals(className)) {
				return innerClass;
			}
		}
		return null;
	}

	/**
	 * Returns the specified class
	 * 
	 * @param fullClassName
	 *            The full qualified name of the class
	 * @return The class, or null if it doesn't exist
	 */
	public static Class<?> getClass(String fullClassName) {
		try {
			return Class.forName(fullClassName);
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Returns the specified field of a class
	 * 
	 * @param theClass
	 *            The class
	 * @param fieldName
	 *            The name of the field
	 * @return The field, or null if it doesn't exist
	 */
	public static Field getField(Class<?> theClass, String fieldName) {
		try {
			Field theField = theClass.getDeclaredField(fieldName);
			theField.setAccessible(true);

			Field modifiers = Field.class.getDeclaredField("modifiers");
			modifiers.setAccessible(true);
			modifiers.set(theField, theField.getModifiers() & ~Modifier.FINAL);

			return theField;
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Returns the specified constructor of a class
	 * 
	 * @param theClass
	 *            The class
	 * @param parameters
	 *            The parameters of the constructor
	 * @return The constructor, or null if it doesn't exist
	 */
	public static <T> Constructor<T> getConstructor(Class<T> theClass, Class<?>... parameters) {
		try {
			Constructor<T> theConstructor = theClass.getDeclaredConstructor(parameters);
			theConstructor.setAccessible(true);
			return theConstructor;
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Returns the specified method of a class
	 * 
	 * @param theClass
	 *            The class
	 * @param methodName
	 *            The name of the method
	 * @param parameters
	 *            The parameters of the method
	 * @return The method, or null if it doesn't exist
	 */
	public static Method getMethod(Class<?> theClass, String methodName, Class<?>... parameters) {
		try {
			Method theMethod = theClass.getDeclaredMethod(methodName, parameters);
			theMethod.setAccessible(true);
			return theMethod;
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Constructs and returns a new object with the given constructor
	 * 
	 * @param theConstructor
	 *            The constructor
	 * @param arguments
	 *            The arguments
	 * @return The new object, or null if anything goes wrong
	 */
	public static Object createInstance(Constructor<?> theConstructor, Object... arguments) {
		try {
			return theConstructor.newInstance(arguments);
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Invokes the given method and returns the result
	 * 
	 * @param theMethod
	 *            The method
	 * @param instance
	 *            The instance that the method will be called upon
	 * @param arguments
	 *            The arguments
	 * @return The result of the invocation, or null if anything goes wrong or
	 *         the method has a void return type
	 */
	public static Object invokeMethod(Method theMethod, Object instance, Object... arguments) {
		try {
			return theMethod.invoke(instance, arguments);
		} catch (Exception ignored) {
			return null;
		}
	}

	/**
	 * Returns the value of the given field
	 * 
	 * @param theField
	 *            The field
	 * @param instance
	 *            The instance that the field will be gotten
	 * @return The value of the field, or null if anything goes wrong, or the
	 *         field holds null
	 */
	public static Object getValue(Field theField, Object instance) {
		try {
			return theField.get(instance);
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Sets the value of the given field
	 * 
	 * @param theField
	 *            The field
	 * @param instance
	 *            The instance that the field will be set
	 * @param value
	 *            The new value
	 * @return Whether the operation was successful or not
	 */
	public static boolean setValue(Field theField, Object instance, Object value) {
		try {
			theField.set(instance, value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
