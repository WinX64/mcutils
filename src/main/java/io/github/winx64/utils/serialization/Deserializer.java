package io.github.winx64.utils.serialization;

/**
 * An interface for deserialization
 * 
 * @author WinX64
 *
 * @param <N>
 *            The normal data type
 * @param <S>
 *            The serialized data type
 */
public interface Deserializer<N, S> {

	/**
	 * Deserializes the given data
	 * 
	 * @param manager
	 *            The {@link SerializationManager} this deserializer is
	 *            registered in
	 * @param serializedData
	 *            The serialized data to be deserialized
	 * @return The normal data
	 */
	public N deserialize(SerializationManager manager, S serializedData);
}
