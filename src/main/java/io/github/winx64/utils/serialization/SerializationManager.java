package io.github.winx64.utils.serialization;

import java.io.DataInput;
import java.io.DataOutput;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.base.Preconditions;

import io.github.winx64.utils.serialization.defaults.InventoryByteArraySerializer;
import io.github.winx64.utils.serialization.defaults.ItemStackByteArraySerializer;
import io.github.winx64.utils.serialization.defaults.ItemStackDataIOSerializer;

public final class SerializationManager {

	private final Map<Class<?>, Map<Class<?>, Serializer<?, ?>>> registeredSerializers;
	private final Map<Class<?>, Map<Class<?>, Deserializer<?, ?>>> registeredDeserializers;

	public SerializationManager() {
		this.registeredSerializers = new HashMap<Class<?>, Map<Class<?>, Serializer<?, ?>>>();
		this.registeredDeserializers = new HashMap<Class<?>, Map<Class<?>, Deserializer<?, ?>>>();

		registerDefaults();
	}

	public <N, S, D> void registerAdvancedDualSerializer(Class<N> normalClass, Class<S> serializerOutputClass,
			Class<D> deserializerInputClass, AdvancedDualSerializer<N, S, D> dualSerializer) {
		registerSerializer(normalClass, serializerOutputClass, dualSerializer);
		registerDeserializer(normalClass, deserializerInputClass, dualSerializer);
	}

	public <N, S> void registerSimpleDualSerializer(Class<N> normalClass, Class<S> serializedClass,
			SimpleDualSerializer<N, S> dualSerializer) {
		registerSerializer(normalClass, serializedClass, dualSerializer);
		registerDeserializer(normalClass, serializedClass, dualSerializer);
	}

	public <N, S> void registerSerializer(Class<N> normalClass, Class<S> serializedClass, Serializer<N, S> serializer) {
		Preconditions.checkNotNull(normalClass, "normalClass cannot be null");
		Preconditions.checkNotNull(serializedClass, "serializedClass cannot be null");
		Preconditions.checkNotNull(serializer, "serializer cannot be null");

		Map<Class<?>, Serializer<?, ?>> serializerMap = registeredSerializers.get(normalClass);
		if (serializerMap == null) {
			registeredSerializers.put(normalClass, serializerMap = new HashMap<Class<?>, Serializer<?, ?>>());
		}

		serializerMap.put(serializedClass, serializer);
	}

	public <N, S> void registerDeserializer(Class<N> normalClass, Class<S> serializedClass,
			Deserializer<N, S> deserializer) {
		Preconditions.checkNotNull(serializedClass, "serializedClass cannot be null");
		Preconditions.checkNotNull(normalClass, "normalClass cannot be null");
		Preconditions.checkNotNull(deserializer, "deserializer cannot be null");

		Map<Class<?>, Deserializer<?, ?>> deserializerMap = registeredDeserializers.get(serializedClass);
		if (deserializerMap == null) {
			registeredDeserializers.put(serializedClass, deserializerMap = new HashMap<Class<?>, Deserializer<?, ?>>());
		}

		deserializerMap.put(normalClass, deserializer);
	}

	public <N, S> S serialize(N normalData, Class<S> serializedClass) {
		return serialize(normalData, serializedClass, true);
	}

	public <N, S> S serialize(N normalData, Class<S> serializedClass, boolean scanParent) {
		Preconditions.checkNotNull(normalData, "normalData cannot be null");
		Preconditions.checkNotNull(serializedClass, "serializedClass cannot be null");

		Class<?> normalClass = normalData.getClass();
		Map<Class<?>, Serializer<?, ?>> serializerMap = registeredSerializers
				.get(scanParent ? findParentClass(registeredSerializers, normalClass) : normalClass);
		Serializer<?, ?> rawSerializer = serializerMap == null ? null
				: serializerMap.get(scanParent ? findParentClass(serializerMap, serializedClass) : serializedClass);
		Preconditions.checkArgument(rawSerializer != null,
				"No registered serializers found for Normal: %s, Serialized: %s", normalClass.getSimpleName(),
				serializedClass.getSimpleName());

		@SuppressWarnings("unchecked")
		Serializer<N, S> serializer = (Serializer<N, S>) rawSerializer;

		try {
			return serializer.serialize(this, normalData);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public <N, S> N deserialize(S serializedData, Class<N> normalClass) {
		return deserialize(serializedData, normalClass, true);
	}

	public <N, S> N deserialize(S serializedData, Class<N> normalClass, boolean scanParent) {
		Preconditions.checkNotNull(serializedData, "serializedData cannot be null");
		Preconditions.checkNotNull(normalClass, "normalClass cannot be null");

		Class<?> serializedClass = serializedData.getClass();
		Map<Class<?>, Deserializer<?, ?>> deserializerMap = registeredDeserializers
				.get(scanParent ? findParentClass(registeredDeserializers, serializedClass) : serializedClass);
		Deserializer<?, ?> rawDeserializer = deserializerMap == null ? null
				: deserializerMap.get(scanParent ? findParentClass(deserializerMap, normalClass) : normalClass);
		Preconditions.checkArgument(rawDeserializer != null,
				"No registered deserializers found for Serialized: %s, Normal: %s", serializedClass.getSimpleName(),
				normalClass.getSimpleName());

		@SuppressWarnings("unchecked")
		Deserializer<N, S> deserializer = (Deserializer<N, S>) rawDeserializer;

		try {
			return deserializer.deserialize(this, serializedData);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void registerDefaults() {
		this.registerSimpleDualSerializer(ItemStack.class, byte[].class, new ItemStackByteArraySerializer());
		this.registerAdvancedDualSerializer(ItemStack.class, DataOutput.class, DataInput.class,
				new ItemStackDataIOSerializer());
		this.registerSimpleDualSerializer(Inventory.class, byte[].class, new InventoryByteArraySerializer());
	}

	private Class<?> findParentClass(Map<Class<?>, ?> map, Class<?> theClass) {
		for (Class<?> targetClass : map.keySet()) {
			if (targetClass.isAssignableFrom(theClass)) {
				return targetClass;
			}
		}

		return null;
	}
}
