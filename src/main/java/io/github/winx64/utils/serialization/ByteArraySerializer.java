package io.github.winx64.utils.serialization;

/**
 * A simple dual serializer that converts normal data into a byte array
 * 
 * @author WinX64
 *
 * @param <N>
 *            The normal data type
 */
public interface ByteArraySerializer<N> extends SimpleDualSerializer<N, byte[]> {

	@Override
	public byte[] serialize(SerializationManager manager, N normalData);

	@Override
	public N deserialize(SerializationManager manager, byte[] serializedData);
}
