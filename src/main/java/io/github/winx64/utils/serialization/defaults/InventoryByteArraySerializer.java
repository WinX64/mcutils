package io.github.winx64.utils.serialization.defaults;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import io.github.winx64.utils.serialization.ByteArraySerializer;
import io.github.winx64.utils.serialization.SerializationManager;

/**
 * A serializer for {@link Inventory} to byte[] conversion
 * 
 * @author WinX64
 *
 */
public final class InventoryByteArraySerializer implements ByteArraySerializer<Inventory> {

	private static final ItemStack AIR = new ItemStack(Material.AIR);

	@Override
	public byte[] serialize(SerializationManager manager, Inventory formattedData) {
		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		DataOutputStream output = new DataOutputStream(bytes);
		try {
			output.writeByte(formattedData.getSize());
			output.writeUTF(formattedData.getTitle());
			ItemStack[] contents = formattedData.getContents();

			for (ItemStack itemStack : contents) {
				if (itemStack == null) {
					itemStack = AIR;
				}
				byte[] itemStackData = manager.serialize(itemStack, byte[].class);
				output.write(itemStackData);
			}
		} catch (IOException ignored) {}

		return bytes.toByteArray();
	}

	@Override
	public Inventory deserialize(SerializationManager manager, byte[] rawData) {
		ByteArrayInputStream bytes = new ByteArrayInputStream(rawData);
		DataInputStream input = new DataInputStream(bytes);
		Inventory inventory = null;

		try {
			inventory = Bukkit.createInventory(null, input.readByte(), input.readUTF());

			for (int i = 0; i < inventory.getSize(); i++) {
				inventory.setItem(i, manager.deserialize(input, ItemStack.class));
			}
		} catch (IOException ignored) {}

		return inventory;
	}
}
