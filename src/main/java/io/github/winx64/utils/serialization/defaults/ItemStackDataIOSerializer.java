package io.github.winx64.utils.serialization.defaults;

import java.io.ByteArrayOutputStream;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BlockStateMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;

import io.github.winx64.utils.reflect.Reflection;
import io.github.winx64.utils.serialization.DataIOSerializer;
import io.github.winx64.utils.serialization.SerializationManager;

/**
 * A serializer for {@link ItemStack} to DataIO conversion
 * 
 * @author WinX64
 *
 */
public final class ItemStackDataIOSerializer implements DataIOSerializer<ItemStack> {

	private static final Class<?> CRAFT_ITEM_META = Reflection.getOBCClass("inventory", "CraftMetaItem");

	private static final Field REPAIR_COST = Reflection.getField(CRAFT_ITEM_META, "repairCost");

	@Override
	public DataOutput serialize(SerializationManager manager, ItemStack formattedData) {
		byte[] bytes = manager.serialize(formattedData, byte[].class);
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		try {
			output.write(bytes);
		} catch (IOException ignored) {}

		return new DataOutputStream(output);
	}

	@SuppressWarnings("deprecation")
	@Override
	public ItemStack deserialize(SerializationManager manager, DataInput input) {
		ItemStack itemStack = null;

		try {
			itemStack = new ItemStack(input.readShort());
			if (itemStack.getType() != Material.AIR) {
				itemStack.setAmount(input.readByte());
				itemStack.setDurability(input.readShort());
				itemStack.getData().setData(input.readByte());

				if (input.readBoolean()) {
					ItemMeta meta = itemStack.getItemMeta();
					if (input.readBoolean()) {
						meta.setDisplayName(input.readUTF());
					}
					List<String> lore = new ArrayList<String>();
					for (int i = 0, loreSize = input.readByte(); i < loreSize; i++) {
						lore.add(input.readUTF());
					}
					meta.setLore(lore);
					for (int i = 0, enchantsSize = input.readByte(); i < enchantsSize; i++) {
						meta.addEnchant(Enchantment.getById(input.readByte()), (int) input.readShort(), true);
					}

					Reflection.setValue(REPAIR_COST, meta, input.readInt());

					if (meta instanceof BlockStateMeta) {
						BlockStateMeta blockState = (BlockStateMeta) meta;
					} else if (meta instanceof LeatherArmorMeta) {
						LeatherArmorMeta leatherArmor = (LeatherArmorMeta) meta;
					} else if (meta instanceof MapMeta) {
						MapMeta map = (MapMeta) meta;
					} else if (meta instanceof PotionMeta) {
						PotionMeta potion = (PotionMeta) meta;
					} else if (meta instanceof BookMeta) {
						BookMeta book = (BookMeta) meta;
					} else if (meta instanceof SkullMeta) {
						SkullMeta skull = (SkullMeta) meta;
					} else if (meta instanceof FireworkMeta) {
						FireworkMeta firework = (FireworkMeta) meta;
					} else if (meta instanceof FireworkEffectMeta) {
						FireworkEffectMeta fireworkEffect = (FireworkEffectMeta) meta;
					} else if (meta instanceof EnchantmentStorageMeta) {
						EnchantmentStorageMeta enchantStorage = (EnchantmentStorageMeta) meta;
					} else if (meta instanceof BannerMeta) {
						BannerMeta banner = (BannerMeta) meta;
					}
					itemStack.setItemMeta(meta);
				}
			}
		} catch (IOException ignored) {}

		return itemStack;
	}
}
