package io.github.winx64.utils.serialization;

import java.io.DataInput;
import java.io.DataOutput;

/**
 * An advanced dual serializer that outputs serialized data in the form of a
 * {@link DataOutput} and receives serialized data in the form of a
 * {@link DataInput}
 * 
 * @author WinX64
 *
 * @param <N>
 *            The normal data type
 */
public interface DataIOSerializer<N> extends AdvancedDualSerializer<N, DataOutput, DataInput> {

	@Override
	public DataOutput serialize(SerializationManager manager, N normalData);

	@Override
	public N deserialize(SerializationManager manager, DataInput serializedData);
}
