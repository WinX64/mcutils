package io.github.winx64.utils.serialization;

/**
 * A dual serializer that allows serializing and deserializing of the same data
 * type, with a different output for serialization and input for deserialization
 * 
 * @author WinX64
 *
 * @param <N>
 *            The normal data type
 * @param <S>
 *            The serializer output data type
 * @param <D>
 *            The deserializer input data type
 */
public interface AdvancedDualSerializer<N, S, D> extends Serializer<N, S>, Deserializer<N, D> {

	@Override
	public S serialize(SerializationManager manager, N normalData);

	@Override
	public N deserialize(SerializationManager manager, D serializedData);
}
