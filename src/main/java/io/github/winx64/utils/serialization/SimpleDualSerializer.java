package io.github.winx64.utils.serialization;

/**
 * A dual serializer that allows serializing and deserializing of the same data
 * type, with the same output for serialization and input for deserialization
 * 
 * @author WinX64
 *
 * @param <N>
 *            The result data type
 * @param <S>
 *            The serializer output data type
 */
public interface SimpleDualSerializer<N, S> extends AdvancedDualSerializer<N, S, S> {

	@Override
	public S serialize(SerializationManager manager, N normalData);

	@Override
	public N deserialize(SerializationManager manager, S serializedData);
}
