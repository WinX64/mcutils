package io.github.winx64.utils.serialization.defaults;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BannerMeta;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.FireworkEffectMeta;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.MapMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import io.github.winx64.utils.reflect.Reflection;
import io.github.winx64.utils.serialization.ByteArraySerializer;
import io.github.winx64.utils.serialization.SerializationManager;

/**
 * A serializer for {@link ItemStack} to byte[] conversion
 * 
 * @author WinX64
 *
 */
public final class ItemStackByteArraySerializer implements ByteArraySerializer<ItemStack> {

	private static final Class<?> CRAFT_ITEM_META = Reflection.getOBCClass("inventory", "CraftMetaItem");

	private static final Field REPAIR_COST = Reflection.getField(CRAFT_ITEM_META, "repairCost");

	private static final ItemStack AIR = new ItemStack(Material.AIR);

	@SuppressWarnings("deprecation")
	@Override
	public byte[] serialize(SerializationManager manager, ItemStack formattedData) {
		if (formattedData == null) {
			formattedData = AIR;
		}

		ByteArrayOutputStream bytes = new ByteArrayOutputStream();
		DataOutputStream output = new DataOutputStream(bytes);

		try {
			output.writeShort(formattedData.getTypeId());
			if (formattedData.getType() != Material.AIR) {
				output.writeByte(formattedData.getAmount());
				output.writeShort(formattedData.getDurability());
				output.writeByte(formattedData.getData().getData());

				output.writeBoolean(formattedData.hasItemMeta());
				if (formattedData.hasItemMeta()) {
					ItemMeta meta = formattedData.getItemMeta();
					output.writeBoolean(meta.hasDisplayName());
					if (meta.hasDisplayName()) {
						output.writeUTF(meta.getDisplayName());
					}
					List<String> lore = meta.getLore();
					output.writeByte(lore == null ? 0 : Math.min(lore.size(), Byte.MAX_VALUE));
					if (lore != null) {
						for (int i = 0; i < Math.min(lore.size(), Byte.MAX_VALUE); i++) {
							output.writeUTF(lore.get(i));
						}
					}
					Map<Enchantment, Integer> enchants = meta.getEnchants();
					output.writeByte(enchants == null ? 0 : Math.min(enchants.size(), Byte.MAX_VALUE));
					if (enchants != null) {
						for (Entry<Enchantment, Integer> enchant : enchants.entrySet()) {
							output.writeByte(enchant.getKey().getId());
							output.writeShort(enchant.getValue());
						}
					}

					output.writeInt((int) Reflection.getValue(REPAIR_COST, meta));

					if (meta instanceof LeatherArmorMeta) {
						LeatherArmorMeta leatherArmor = (LeatherArmorMeta) meta;
						output.writeInt(leatherArmor.getColor().asRGB());
					} else if (meta instanceof MapMeta) {
						MapMeta map = (MapMeta) meta;
						output.writeBoolean(map.isScaling());
					} else if (meta instanceof PotionMeta) {
						PotionMeta potion = (PotionMeta) meta;
						List<PotionEffect> effects = potion.getCustomEffects();
						output.writeByte(effects == null ? 0 : Math.min(effects.size(), Byte.MAX_VALUE));
						for (int i = 0; i < Math.min(effects.size(), Byte.MAX_VALUE); i++) {
							PotionEffect effect = effects.get(i);
							output.writeByte(effect.getType().getId());
							output.writeInt(effect.getDuration());
							output.writeByte(effect.getAmplifier());
							output.writeBoolean(effect.isAmbient());
							output.writeBoolean(effect.hasParticles());
						}
					} else if (meta instanceof BookMeta) {
						BookMeta book = (BookMeta) meta;
						output.writeBoolean(book.hasTitle());
						if (book.hasTitle()) {
							output.writeUTF(book.getTitle());
						}
						output.writeBoolean(book.hasAuthor());
						if (book.hasAuthor()) {
							output.writeUTF(book.getAuthor());
						}
						List<String> pages = book.getPages();
						output.writeByte(pages == null ? 0 : Math.min(pages.size(), Byte.MAX_VALUE));
						for (int i = 0; i < Math.min(pages.size(), Byte.MAX_VALUE); i++) {
							output.writeUTF(pages.get(i));
						}

					} else if (meta instanceof SkullMeta) {
						SkullMeta skull = (SkullMeta) meta;
						output.writeBoolean(skull.hasOwner());
						if (skull.hasOwner()) {
							output.writeUTF(skull.getOwner());
						}
					} else if (meta instanceof FireworkMeta) {
						FireworkMeta firework = (FireworkMeta) meta;
					} else if (meta instanceof FireworkEffectMeta) {
						FireworkEffectMeta fireworkEffect = (FireworkEffectMeta) meta;
					} else if (meta instanceof EnchantmentStorageMeta) {
						EnchantmentStorageMeta enchantStorage = (EnchantmentStorageMeta) meta;
					} else if (meta instanceof BannerMeta) {
						BannerMeta banner = (BannerMeta) meta;
					}
				}
			}
		} catch (IOException ignored) {}

		return bytes.toByteArray();
	}

	@Override
	public ItemStack deserialize(SerializationManager manager, byte[] rawData) {
		ByteArrayInputStream bytes = new ByteArrayInputStream(rawData);
		DataInputStream input = new DataInputStream(bytes);
		return manager.deserialize(input, ItemStack.class);
	}
}
