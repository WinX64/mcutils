package io.github.winx64.utils.serialization;

/**
 * An interface for serialization
 * 
 * @author WinX64
 *
 * @param <N>
 *            The normal data type
 * @param <S>
 *            The serialized data type
 */
public interface Serializer<N, S> {

	/**
	 * Serializes the given data
	 * 
	 * @param manager
	 *            The {@link SerializationManager} this serializer is registered
	 *            in
	 * @param normalData
	 *            The normal data to be serialized
	 * @return The serialized data
	 */
	public S serialize(SerializationManager manager, N normalData);
}
